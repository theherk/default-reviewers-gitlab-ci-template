# Default Reviewers Gitlab CI Template

Very simple template to add default reviewers to created and updated merge requests. Here is [the epic](https://gitlab.com/groups/gitlab-org/-/epics/4367#note_1407504353) tracking the feature that will maybe be implemented someday. In the meantime, this is pretty simple to use.

This will append the default reviewers to the currently set reviewers rather than replacing.

## Requirements

- A token that allows access to the Gitlab API.
- A variable `API TOKEN` storing that token.
- Import this template.

## Usage

First, create a token with `API` access. This can be a [Personal Access Token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) or a [Project Access Token](https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html).

In your CI/CD Settings, set the following variable:

- `API_TOKEN`: This is a token with API access (private or personal).

This *cannot* be a `CI_JOB_TOKEN`. That token has very limited access and cannot update merge requests.

Import this template and add `DEFAULT_REVIEWERS` to .gitlab-ci.yml:

``` yaml
include:
  - remote: "https://gitlab.com/theherk/default-reviewers-gitlab-ci-template/-/raw/main/.default-reviewers.yml"

variables:
  DEFAULT_REVIEWERS: |
    Adam.Sherwood
    Some.Other.User
```

Then, just ensure you have a `build` stage. Those are part of the [default stages](https://docs.gitlab.com/ee/ci/yaml/#stages) if none are given, but if you have other stages defined, you'll need to add them explicitly.

``` yaml
stages:
  - ...
  - build
```

That's it. Now, merge requests will have the users added as reviewers.

## Images Used

- [badouralix/curl-jq](https://hub.docker.com/r/badouralix/curl-jq#!)
